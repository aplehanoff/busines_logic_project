# README #

Тестовое задание для собеседования в ООО "Логика Бизнеса"

### Технологии, использованнные при разработке  ###

* Apache Tomcat 8.5.16
* Spring Boot 1.5.4.RELEASE
* Maven 3.3.9

### Сборка/Запуск ###
#### БД
* Создать пользователя БД `login: bl-user/ passworn: bl-user`
* Восстановить тестовую БД из файла `src/main/resources/sql/dump.sql`
#### Сервер
* В корне проекта выполнить команду `mvn clean package`. Либо взять имеющийся файл в `target/structure.war`
* Использовать собранный `target/structure.war` для запуска в локальном `apache`

##### Альтернативный способ

* В корне выполнить команду `mvn clean package`.
* Использовать для запуска `Spring Boot` командой `mvn spring-boot:run`