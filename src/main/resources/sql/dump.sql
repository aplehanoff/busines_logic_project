--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.7
-- Dumped by pg_dump version 9.5.7

-- Started on 2017-08-09 12:26:06 MSK

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2178 (class 1262 OID 1465656)
-- Name: structure-db; Type: DATABASE; Schema: -; Owner: bl-user
--

CREATE DATABASE "structure-db" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE "structure-db" OWNER TO "bl-user";

\connect -reuse-previous=on "dbname='structure-db'"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12395)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2181 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 182 (class 1259 OID 1465659)
-- Name: customers; Type: TABLE; Schema: public; Owner: bl-user
--

CREATE TABLE customers (
    id bigint NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    secondname character varying(255),
    birth date,
    email character varying(255) NOT NULL,
    phone character varying(255),
    id_depart bigint NOT NULL,
    id_owned bigint
);


ALTER TABLE customers OWNER TO "bl-user";

--
-- TOC entry 181 (class 1259 OID 1465657)
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: bl-user
--

CREATE SEQUENCE customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customers_id_seq OWNER TO "bl-user";

--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 181
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bl-user
--

ALTER SEQUENCE customers_id_seq OWNED BY customers.id;


--
-- TOC entry 184 (class 1259 OID 1465672)
-- Name: departments; Type: TABLE; Schema: public; Owner: bl-user
--

CREATE TABLE departments (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    id_office bigint NOT NULL,
    id_parent bigint
);


ALTER TABLE departments OWNER TO "bl-user";

--
-- TOC entry 183 (class 1259 OID 1465670)
-- Name: departments_id_seq; Type: SEQUENCE; Schema: public; Owner: bl-user
--

CREATE SEQUENCE departments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE departments_id_seq OWNER TO "bl-user";

--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 183
-- Name: departments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bl-user
--

ALTER SEQUENCE departments_id_seq OWNED BY departments.id;


--
-- TOC entry 186 (class 1259 OID 1465687)
-- Name: offices; Type: TABLE; Schema: public; Owner: bl-user
--

CREATE TABLE offices (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    address character varying(255)
);


ALTER TABLE offices OWNER TO "bl-user";

--
-- TOC entry 185 (class 1259 OID 1465685)
-- Name: offices_id_seq; Type: SEQUENCE; Schema: public; Owner: bl-user
--

CREATE SEQUENCE offices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE offices_id_seq OWNER TO "bl-user";

--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 185
-- Name: offices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bl-user
--

ALTER SEQUENCE offices_id_seq OWNED BY offices.id;


--
-- TOC entry 2033 (class 2604 OID 1465662)
-- Name: id; Type: DEFAULT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY customers ALTER COLUMN id SET DEFAULT nextval('customers_id_seq'::regclass);


--
-- TOC entry 2034 (class 2604 OID 1465675)
-- Name: id; Type: DEFAULT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY departments ALTER COLUMN id SET DEFAULT nextval('departments_id_seq'::regclass);


--
-- TOC entry 2035 (class 2604 OID 1465690)
-- Name: id; Type: DEFAULT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY offices ALTER COLUMN id SET DEFAULT nextval('offices_id_seq'::regclass);


--
-- TOC entry 2169 (class 0 OID 1465659)
-- Dependencies: 182
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: bl-user
--



--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 181
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bl-user
--

SELECT pg_catalog.setval('customers_id_seq', 3, true);


--
-- TOC entry 2171 (class 0 OID 1465672)
-- Dependencies: 184
-- Data for Name: departments; Type: TABLE DATA; Schema: public; Owner: bl-user
--



--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 183
-- Name: departments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bl-user
--

SELECT pg_catalog.setval('departments_id_seq', 1, true);


--
-- TOC entry 2173 (class 0 OID 1465687)
-- Dependencies: 186
-- Data for Name: offices; Type: TABLE DATA; Schema: public; Owner: bl-user
--



--
-- TOC entry 2187 (class 0 OID 0)
-- Dependencies: 185
-- Name: offices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bl-user
--

SELECT pg_catalog.setval('offices_id_seq', 4, true);


--
-- TOC entry 2037 (class 2606 OID 1465771)
-- Name: c_email_unic; Type: CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT c_email_unic UNIQUE (email);


--
-- TOC entry 2039 (class 2606 OID 1465684)
-- Name: c_id_unic; Type: CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT c_id_unic UNIQUE (id);


--
-- TOC entry 2041 (class 2606 OID 1465667)
-- Name: c_pkey; Type: CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT c_pkey PRIMARY KEY (id);


--
-- TOC entry 2043 (class 2606 OID 1465737)
-- Name: d_name_unic; Type: CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY departments
    ADD CONSTRAINT d_name_unic UNIQUE (name);


--
-- TOC entry 2045 (class 2606 OID 1465680)
-- Name: d_pkey; Type: CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY departments
    ADD CONSTRAINT d_pkey PRIMARY KEY (id);


--
-- TOC entry 2047 (class 2606 OID 1465727)
-- Name: o_name_unic; Type: CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY offices
    ADD CONSTRAINT o_name_unic UNIQUE (name);


--
-- TOC entry 2049 (class 2606 OID 1465695)
-- Name: o_pkey; Type: CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY offices
    ADD CONSTRAINT o_pkey PRIMARY KEY (id);


--
-- TOC entry 2050 (class 2606 OID 1465708)
-- Name: c_d_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT c_d_fkey FOREIGN KEY (id_depart) REFERENCES departments(id) ON DELETE CASCADE;


--
-- TOC entry 2051 (class 2606 OID 1465713)
-- Name: c_d_owned_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT c_d_owned_fkey FOREIGN KEY (id_owned) REFERENCES departments(id);


--
-- TOC entry 2053 (class 2606 OID 1465703)
-- Name: d_d_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY departments
    ADD CONSTRAINT d_d_fkey FOREIGN KEY (id_parent) REFERENCES departments(id) ON DELETE CASCADE;


--
-- TOC entry 2052 (class 2606 OID 1465698)
-- Name: d_o_fkey; Type: FK CONSTRAINT; Schema: public; Owner: bl-user
--

ALTER TABLE ONLY departments
    ADD CONSTRAINT d_o_fkey FOREIGN KEY (id_office) REFERENCES offices(id);


--
-- TOC entry 2180 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-08-09 12:26:06 MSK

--
-- PostgreSQL database dump complete
--

