package me.aplehanoff.structure.model;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by user on 02.08.17.
 */
@Entity
@Table(name="departments")
public class Department {
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "departments_id_seq")
    @SequenceGenerator(name="departments_id_seq", sequenceName="departments_id_seq", allocationSize=1)
    private Long id;
    
    @Column(name="name")
    private String name;
    
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "ownedDepartment")
    private Customer owner;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_office", referencedColumnName="id")
    private Office office;

    @OneToMany(orphanRemoval = true, mappedBy = "department")
    private Set<Department> child;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_parent", referencedColumnName="id")
    private Department department;
    
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(insertable = false, updatable = false, name="id_parent", referencedColumnName="id")
    private List<Department> subDepartments;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(insertable = false, updatable = false, name="id_depart", referencedColumnName="id")
    private List<Customer> customers;

    public Department() {
    }

    public Department(String name, Customer owner, Office office, Department department, List<Department> subDepartments, List<Customer> customers) {
        this.name = name;
        this.owner = owner;
        this.office = office;
        this.department = department;
        this.subDepartments = subDepartments;
        this.customers = customers;
    }

    public Department(String name, Office office, Department department) {
        this.name = name;
        this.office = office;
        this.department = department;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Set<Department> getChild() {
        return child;
    }

    public void setChild(Set<Department> child) {
        this.child = child;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<Department> getSubDepartments() {
        return subDepartments;
    }

    public void setSubDepartments(List<Department> subDepartments) {
        this.subDepartments = subDepartments;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }
}
