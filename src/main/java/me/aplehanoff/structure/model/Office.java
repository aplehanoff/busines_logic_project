package me.aplehanoff.structure.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by user on 02.08.17.
 */
@Entity
@Table(name="offices")
public class Office {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "offices_id_seq")
    @SequenceGenerator(name="offices_id_seq", sequenceName="offices_id_seq", allocationSize=1)
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="address")
    private String address;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(insertable = false, updatable = false, name = "id_office", referencedColumnName="id")
    private List<Department> departments;

    public Office() {
    }

    public Office(String name, String address, List<Department> departments) {
        this.name = name;
        this.address = address;
        this.departments = departments;
    }

    public Office(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
}
