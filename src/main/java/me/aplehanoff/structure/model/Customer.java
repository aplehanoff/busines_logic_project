package me.aplehanoff.structure.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by user on 02.08.17.
 */
@Entity
@Table(name="customers")
public class Customer {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customers_id_seq")
    @SequenceGenerator(name="customers_id_seq", sequenceName="customers_id_seq", allocationSize=1)
    private Long id;

    @Column(name="lastname")
    private String lastname;

    @Column(name="firstname")
    private String firstname;

    @Column(name="secondname")
    private String secondname;

    @Column(name="birth")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birth;

    @Column(name="email", unique = true)
    private String email;

    @Column(name = "phone")
    private String phone;

    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name="id_depart", referencedColumnName="id")
    private Department department;

    @OneToOne
    @JoinColumn(name="id_owned")
    private Department ownedDepartment;

    public Customer() {
    }

    public Customer(String lastname, String firstname, String secondname, Date birth, String email, String phone, Department department, Department ownedDepartment) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.secondname = secondname;
        this.birth = birth;
        this.email = email;
        this.phone = phone;
        this.department = department;
        this.ownedDepartment = ownedDepartment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Department getOwnedDepartment() {
        return ownedDepartment;
    }

    public void setOwnedDepartment(Department ownedDepartment) {
        this.ownedDepartment = ownedDepartment;
    }
}
