package me.aplehanoff.structure.repo;

import me.aplehanoff.structure.model.Department;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by user on 02.08.17.
 */
public interface DepartmentRepo extends CrudRepository<Department, Long> {
    Department findById(Long id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Department as d set d.name = :name where d.id = :id")
    void updateById(@Param("id") Long id, @Param("name") String name);

    Integer countByNameIgnoreCase(String name);

    Integer countByNameIgnoreCaseAndIdNot(String name, Long id);
}
