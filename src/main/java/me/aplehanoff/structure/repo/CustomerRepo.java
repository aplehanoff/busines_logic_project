package me.aplehanoff.structure.repo;

import me.aplehanoff.structure.model.Customer;
import me.aplehanoff.structure.model.Department;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

/**
 * Created by user on 02.08.17.
 */
public interface CustomerRepo extends CrudRepository<Customer, Long> {
    Customer findById(Long id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Customer c set c.lastname = :lastname, c.firstname = :firstname, c.secondname = :secondname, c.birth= :birth, c.email = :email, c.phone = :phone where c.id = :id")
    void updateById(@Param("id") Long id,
                    @Param("lastname") String lastname,
                    @Param("firstname") String firstname,
                    @Param("secondname") String secondname,
                    @Param("birth") Date birth,
                    @Param("email") String email,
                    @Param("phone") String phone);

    Integer countByEmailAndIdNot(String email, Long id);

    Integer countByEmail(String email);

    List<Customer> findByDepartment(Department department);
}
