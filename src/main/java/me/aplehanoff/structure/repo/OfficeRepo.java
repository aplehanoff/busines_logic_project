package me.aplehanoff.structure.repo;

import me.aplehanoff.structure.model.Office;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by user on 02.08.17.
 */
public interface OfficeRepo extends CrudRepository<Office, Long> {

    List<Office> findAll();

    Office findById(Long id);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Office o set o.name = :name, o.address = :address where o.id = :id")
    void updateById(@Param("id") Long id, @Param("name") String name, @Param("address") String address);

    Integer countByNameIgnoreCase(String name);

    Integer countByNameIgnoreCaseAndIdNot(String name, Long id);
}
