package me.aplehanoff.structure.controller;

import me.aplehanoff.structure.model.Office;
import me.aplehanoff.structure.repo.OfficeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.websocket.server.PathParam;
import java.util.List;

@Controller
@Scope("session")
public class OfficeController {

    protected static List<Office> offices;

    @Autowired
    private OfficeRepo officeRepo;
    private Logger logger = Logger.getLogger(OfficeController.class);

    @RequestMapping(value = "/")
    public String init(Model model) {
        logger.info("Application index page filling...");
        offices = officeRepo.findAll();
        model.addAttribute("offices", offices);
        return "index";
    }

    @RequestMapping(value = "/office", method = RequestMethod.GET)
    public String getOfficeInfo(@PathParam(value = "id") Long id,Model model) {
        logger.info("Get info for Office with id= " + id.toString());
        model.addAttribute("offices", offices);
        Office location = officeRepo.findById(id);
        model.addAttribute("location", location);
        return "location";
    }

    @RequestMapping(value = "/office/add", method = RequestMethod.GET)
    public String addOfficeGet(Model model) {
        logger.info("Open adding form for offices");
        model.addAttribute("offices", offices);
        model.addAttribute("addOffice", new Office());
        return "add_office";
    }

    @RequestMapping(value = "/office/add", method = RequestMethod.POST)
    public String addOffice(@ModelAttribute("addOffice") Office office, BindingResult errors, Model model) {
        logger.info("Try to add office");
        model.addAttribute("offices", offices);
        if(office.getName().isEmpty()) {
            logger.error("Office name not specified");
            errors.rejectValue("name","addOffice.name","Укажите название офиса");
            return "add_office";
        } else if(office.getAddress().isEmpty()) {
            logger.error("Office address not specified");
            errors.rejectValue("address", "addOffice.address", "Укажите адрес офиса");
        }

        Integer counter = officeRepo.countByNameIgnoreCase(office.getName());
        if(counter > 0) {
            logger.error("Office with name " + office.getName() + " already exists");
            errors.rejectValue("name","addOffice.name","Офис с данным названием существует");
            return "add_office";
        } else {
            logger.info("Validation passed. Saving office...");
            officeRepo.save(office);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/office/update", method = RequestMethod.POST)
    public String updateOffice(@ModelAttribute("location") Office office, BindingResult errors, Model model) {
        logger.info("Try to update office");
        Integer counter = officeRepo.countByNameIgnoreCaseAndIdNot(office.getName(), office.getId());
        if(counter > 0) {
            logger.error("Office with name " + office.getName() + " already exists");
            model.addAttribute("offices", offices);
            errors.rejectValue("name", "location.name", "Офис с данным названием существует");
            return "location";
        }
        logger.info("Validation passed. Updating office...");
        officeRepo.updateById(office.getId(),office.getName(),office.getAddress());
        return "/";
    }

    @RequestMapping(value = "/office/delete", method = RequestMethod.POST)
    public String deleteOffice(@ModelAttribute("location") Office office) {
        logger.info("Removing office...");
        try {
            officeRepo.delete(office);
        } catch (Throwable t) {
            logger.error(t.getStackTrace());
        }
        return "redirect:/";
    }
}
