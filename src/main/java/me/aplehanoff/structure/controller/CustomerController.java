package me.aplehanoff.structure.controller;

import me.aplehanoff.structure.model.Customer;
import me.aplehanoff.structure.model.Department;
import me.aplehanoff.structure.repo.CustomerRepo;
import me.aplehanoff.structure.repo.DepartmentRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;

@Controller
public class CustomerController {

    @Autowired
    CustomerRepo customerRepo;

    @Autowired
    DepartmentRepo departmentRepo;

    private Logger logger = Logger.getLogger(CustomerController.class);

    @RequestMapping(value = "/customer/add", method = RequestMethod.GET)
    public String addCustomerGet(@PathParam("departmentId") Long departmentId, Model model) {
        logger.info("Open adding form for customers");
        Department department = departmentRepo.findById(departmentId);
        Customer customer = new Customer();
        customer.setDepartment(department);
        model.addAttribute("offices", OfficeController.offices);
        model.addAttribute("locationInfo", department);
        model.addAttribute("addCustomer", customer);
        return "add_customer";
    }

    @RequestMapping(value = "/customer/add", method = RequestMethod.POST)
    public String addCustomer(@PathParam("departmentId") Long departmentId,
                              @PathParam("owner") Boolean owner,
                              @ModelAttribute("addCustomer") Customer customer,
                              BindingResult errors,
                              Model model) {
        logger.info("Try to add customer");
        Department department = departmentRepo.findById(departmentId);
        model.addAttribute("offices", OfficeController.offices);
        model.addAttribute("locationInfo", department);
        if("".equals(customer.getEmail())) customer.setEmail(null);
        if(customer.getEmail() != null){
            Integer counter = customerRepo.countByEmail(customer.getEmail());
            if(counter >0) {
                logger.error("Email " + customer.getEmail() + "is already in use");
                errors.rejectValue("email","addOffice.email","Данный email уже используется");
                return "add_customer";
            }
        }
        customer.setDepartment(department);
        if(owner != null) {
            logger.info("Customer is owner");
            customer.setOwnedDepartment(department);
        }
        logger.info("Validation passed. Saving customer...");
        customerRepo.save(customer);
        return "redirect:/office/department?id="+departmentId;
    }

    @RequestMapping(value = "/customer/update", method = RequestMethod.GET)
    public String getInfoForUpdate(@PathParam("id") Long id, Model model) {
        logger.info("Open updating form for customers");
        Customer customer = customerRepo.findById(id);
        model.addAttribute("offices", OfficeController.offices);
        model.addAttribute("locationInfo",customer.getDepartment());
        model.addAttribute("customerUpdate", customer);
        return "update_customer";
    }

    @RequestMapping(value = "/customer/update", method = RequestMethod.POST)
    public String updateCustomer(@ModelAttribute("customerUpdate") Customer customer, BindingResult errors, Model model) {
        logger.info("Try to update customer");
            Integer counter = customerRepo.countByEmailAndIdNot(customer.getEmail(), customer.getId());
            if(counter >0) {
                logger.error("Email " + customer.getEmail() + "is already in use");
                model.addAttribute("offices", OfficeController.offices);
                model.addAttribute("locationInfo",customer.getDepartment());
                errors.rejectValue("email", "customerUpdate.email", "Данный email уже существует.");
                logger.error("Данный email уже существует.");
                return "update_customer";
            } else {
                logger.info("Validation passed. Updating customer...");
                customerRepo.updateById(customer.getId(),
                        customer.getLastname(),
                        customer.getFirstname(),
                        customer.getSecondname(),
                        customer.getBirth(),
                        customer.getEmail(),
                        customer.getPhone());
            }
        return "redirect:/office/department?id="+customer.getDepartment().getId().toString();
    }

    @RequestMapping(value = "/customer/delete", method = RequestMethod.POST)
    public String deleteCustomer(@ModelAttribute("customer") Customer customer) {
        logger.info("Removing customer...");
        try {
            customerRepo.delete(customer);
        } catch (Throwable t) {
            logger.error(t.getStackTrace());
        }
        return "redirect:/";
    }
}
