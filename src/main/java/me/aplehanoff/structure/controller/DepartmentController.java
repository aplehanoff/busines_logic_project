package me.aplehanoff.structure.controller;

import me.aplehanoff.structure.model.Customer;
import me.aplehanoff.structure.model.Department;
import me.aplehanoff.structure.model.Office;
import me.aplehanoff.structure.repo.CustomerRepo;
import me.aplehanoff.structure.repo.DepartmentRepo;
import me.aplehanoff.structure.repo.OfficeRepo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.websocket.server.PathParam;
import java.util.List;


@Controller
public class DepartmentController {

    @Autowired
    DepartmentRepo departmentRepo;

    @Autowired
    CustomerRepo customerRepo;

    @Autowired
    OfficeRepo officeRepo;

    private Logger logger = Logger.getLogger(DepartmentController.class);

    @RequestMapping(value = "/office/department", method = RequestMethod.GET)
    public String getDepartmentInfo(@PathParam(value = "id") Long id, Model model) {
        logger.info("Get info for Department with id= " + id.toString());
        model.addAttribute("offices", OfficeController.offices);
        Department department = departmentRepo.findById(id);
        List<Customer> customers = department.getCustomers();
        model.addAttribute("location", department);
        model.addAttribute("customers", customers);
        return "location";
    }

    @RequestMapping(value = "/department/add", method = RequestMethod.GET)
    public String addDepartmentGet(@PathParam("officeId") Long officeId,
                                   @PathParam("departmentId") Long departmentId, Model model) {
        logger.info("Open adding form for departments");
        model.addAttribute("offices", OfficeController.offices);

        Office office = officeRepo.findById(officeId);
        Department parent = departmentRepo.findById(departmentId);
        Department department = new Department();
        department.setOffice(office);
        department.setDepartment(parent);
        model.addAttribute("addDepartment", department);
        return "add_department";
    }

    @RequestMapping(value = "/department/add", method = RequestMethod.POST)
    public String addDepartment(@PathParam("officeId") Long officeId,
                                @PathParam("departmentId") Long departmentId,
                                @ModelAttribute("addDepartment") Department department,
                                BindingResult errors, Model model) {
        logger.info("Try to add department");
        model.addAttribute("offices", OfficeController.offices);
        Office office = officeRepo.findById(officeId);
        Department parent = departmentRepo.findById(departmentId);
        department.setOffice(office);
        department.setDepartment(parent);

        if(department.getName().isEmpty()) {
            logger.error("Department name not specified");
            errors.rejectValue("name","addDepartment.name","Укажите название отдела");
            return "add_department";
        }

        Integer counter = departmentRepo.countByNameIgnoreCase(department.getName());
        if(counter > 0) {
            logger.error("Department with name " + department.getName() + " already exists");
            errors.rejectValue("name","addDepartment.name","Отдел с данным названием существует");
            return "add_department";
        }
        logger.info("Validation passed. Saving department...");
        departmentRepo.save(department);
        OfficeController.offices = officeRepo.findAll();
        return "redirect:/";
    }

    @RequestMapping(value = "/department/update", method = RequestMethod.POST)
    public String updateDepartment(@ModelAttribute("location") Department department, BindingResult errors, Model model) {
        logger.info("Try to update department");
        Integer counter = departmentRepo.countByNameIgnoreCaseAndIdNot(department.getName(), department.getId());
        List<Customer> customers = customerRepo.findByDepartment(department);
        if( counter > 0) {
            logger.error("Department with name " + department.getName() + " already exists");
            department.setCustomers(customers);
            model.addAttribute("offices", OfficeController.offices);
            model.addAttribute("customers", customers);
            errors.rejectValue("name", "location.name", "Отдел с таким наименованием существует");
            return "location";
        } else {
            logger.info("Validation passed. Updating department...");
            departmentRepo.updateById(department.getId(), department.getName());
        }
        return "redirect:/office/department?id="+department.getId().toString();
    }

    @RequestMapping(value = "/department/delete", method = RequestMethod.POST)
    public String deleteOffice(@ModelAttribute("location") Department department) {
        logger.info("Removing department...");
        try {
            departmentRepo.delete(department);
        } catch (Throwable t) {
            logger.error(t.getStackTrace());
        }
        return "redirect:/";
    }
}
