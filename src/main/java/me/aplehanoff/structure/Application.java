package me.aplehanoff.structure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by user on 01.08.17.
 */
@SpringBootApplication
@ComponentScan(basePackages = "me.aplehanoff.structure")
@EntityScan(basePackages = "me.aplehanoff.structure.model")
@EnableJpaRepositories(basePackages = "me.aplehanoff.structure.repo")
@PropertySource(value = {"classpath:application.properties","classpath:log4j.properties"}, ignoreResourceNotFound = true)
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
