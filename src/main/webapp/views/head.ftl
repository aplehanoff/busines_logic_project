    <style>
        body, div, p {
            margin:0;
            padding:0;
        }
        h5 {
            font-size: 2.25rem!important;
        }
        .title a, .title a:hover {
            text-decoration: none;
            color: black;
        }
        .add {
            padding:5px;
            margin:20px 5px;
            font-size: 16px!important;
        }

        .full-hght {
            height:100%;
        }
        .half-height {
            min-height: 50%!important;
        }
        .quarter-height {
            min-height:25%!important;
        }
        .location-tree {
            background-color: #f5f5f5;
        }
        .nav-list {
            padding-left: 15px;
            padding-right: 15px;
            margin-bottom: 0;
            margin: 0 0 10px 25px;
        }
        .nav-list > .nav-header {
            margin-left: -15px;
            margin-right: -15px;
            text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
        }

        .nav-header {
            display: block;
            padding: 3px 15px;
            font-size: 16px;
            font-weight: bold;
            line-height: 20px;
            color: #999999;
            text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
            text-transform: uppercase;
        }

        .title {
            float: left;
        }

        .form-wrapper {
            padding: 1.5rem;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
            margin-top: 20px;
            border-width: .2rem;
            border: solid #f7f7f9;
        }

        .form-control.error {
            border-color: red;
        }
        span.error {
            color: red;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="/structure/styles/bootstrap.css" />
    <script  type='text/javascript' src="/structure/javascripts/jquery-3.2.1.min.js"></script>
    <script src="/structure/javascripts/requestController.js"></script>
    <script src="/structure/javascripts/utils.js"></script>
    <script src="/structure/javascripts/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".icon").click(function () {
               var icon =$(this).parent().children(".icon");
               var inner = $(this).parent().children(".inner");
               if(icon.hasClass("plus")) {
                   icon.removeClass("plus").addClass("minus");
                   if(inner.hasClass("hidden")) {
                       inner.removeClass("hidden");
                   }
               } else {
                   icon.removeClass("minus").addClass("plus");
                   if(!inner.hasClass("hidden")) {
                       inner.addClass("hidden");
                   }
               }
            });
        });
    </script>
    <#import "spring.ftl" as spring>
</head>