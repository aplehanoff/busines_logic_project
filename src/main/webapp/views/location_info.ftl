<div class="col-md-8">
    <h3 class="pull-left">Информация</h3>
    <div class="col-md-12">
        <p>Наименование:</p>
        <p>${locationInfo.name}</p>
        <#if locationInfo.department??>
            <p>Головной отдел:</p>
            <p>${locationInfo.department.name}</p>
        </#if>
    </div>
</div>