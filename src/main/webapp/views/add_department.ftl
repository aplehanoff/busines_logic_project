<html>
<head><#include "head.ftl"/>
<body>
<#include "location_tree.ftl"/>
    <div class="col-md-8 half-hght">
        <div class="page-header">
            <h3 id="updateCustomerLable">Добавление отдела</h3>
            <p>Заполните поля формы</p>
        </div>
        <div class="form-wrapper">
            <div id="addDepartment" aria-labelledby="addDepartmentLable" role="dialog" tabindex="-1">
                <form id="departmentAdd" name="departmentAdd" action="/structure/department/add" method="POST">
                        <div class="form-group hidden">
                            <input type="text" name="officeId" class="form-control" id="officeId" readonly="readonly" value="${addDepartment.office.id}">
                        </div>
                        <div class="form-group">
                            <label for="office" class="form-control-label">Офис:</label>
                            <input type="text" name="officeName" class="form-control" id="officeName" readonly="readonly" value="${addDepartment.office.name}">
                        </div>
                    <#if addDepartment.department??>
                        <div class="form-group hidden">
                            <input type="text" name="departmentId" class="form-control" id="departmentId" readonly="readonly" value="${addDepartment.department.id}">
                        </div>
                        <div class="form-group">
                            <label for="department" class="form-control-label">Головной отдел:</label>
                            <input type="text" name="departmentName" class="form-control" id="departmentName" readonly="readonly" value="${addDepartment.department.name}">
                        </div>
                    </#if>
                    <div class="form-group">
                    <@spring.bind "addDepartment.name" />
                        <label for="name" class="form-control-label">Название:</label>
                        <@spring.formInput path="addDepartment.name" attributes="required='required' class='form-control'"/>
                        <@spring.showErrors classOrStyle="error" separator="<br>" />
                    </div>
                </form>
                <a href="/structure/office<#if addDepartment.department??>/department?id=${addDepartment.department.id}<#else>?id=${addDepartment.office.id}</#if>" class="btn btn-default" data-dismiss="modal">Отмена</a>
                <button type="submit" class="btn btn-primary" form="departmentAdd" value="Создать">Создать</button>
            </div>
        </div>
    </div>
</body>
</html>