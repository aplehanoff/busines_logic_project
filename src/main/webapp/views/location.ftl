    <html lang="en">
<head>
    <title>Company structure Office</title>
    <#include "head.ftl">
<body>
    <#include "location_tree.ftl">
    <div class="col-md-8">
        <h3 class="pull-left">Информация</h3>
        <form id="removeLocation" name="removeLocation" action="/structure/<#if location.address??>office<#else>department</#if>/delete?id=${location.id}" method="POST"></form>
        <button title="Удалить" form="removeLocation" type="submit" class="add btn btn-default pull-right"><span role="button">Удалить</span></button>
        <a href="/structure/department/add?officeId=<#if location.address??>${location.id}<#else>${location.office.id}&departmentId=${location.id}</#if>" class="add btn btn-default pull-right"><span role="button" title="Добавить отдел">Добавить отдел</span></a>
        <button type="submit" class="add btn btn-primary pull-right" form="<#if location.address??>officeInfo<#else>departmentInfo</#if>" value="Сохранить"><span role="button" title="Сохранить">Сохранить</span></button>

        <form id="<#if location.address??>officeInfo<#else>departmentInfo</#if>" action="/structure/<#if location.address??>office<#else>department</#if>/update?id=${location.id}" method="post">
            <div class="col-md-12 location-info quarter-height">
                <div class="form-group">
                    <@spring.bind "location.id"/>
                    <@spring.formInput path="location.id" attributes="readonly='readonly' class='hidden'"/>
                    <#if !location.address??>
                        <@spring.bind "location.office.id"/>
                    <@spring.formInput path="location.office.id" attributes="readonly='readonly' class='hidden'"/>
                    </#if>
                </div>
                <div class="form-group">
                    <@spring.bind "location.name"/>
                    <label for="name" class="form-control-label">Наименование:</label>
                    <@spring.formInput path="location.name" attributes="required='required' class='form-control'"/>
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                </div>
            <#if location.address??>
                <div class="form-group">
                    <@spring.bind "location.address"/>
                    <label for="address" class="form-control-label">Адрес:</label>
                    <@spring.formInput path="location.address" attributes="required='required' class='form-control'"/>
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                </div>
            </#if>
            </div>
        </form>
        <div class="col-md-12 half-height">
            <h3 class="pull-left">Сотрудники</h3>
        <#if !location.address??>
            <#if location.customers?has_content>
                <button title="Удалить" onclick="removeCustomer()" type="submit" class="add btn btn-default pull-right"><span role="button">Удалить</span></button>
                <button type="button" class="add btn btn-default pull-right"><span title="Редактировать сотрудника" role="button" onclick="updateCustomerGet()">Редактировать</span></button>
            </#if>
            <a href="/structure/customer/add?departmentId=${location.id}" class="add btn btn-default pull-right"><span title="Добавить сотрудника" role="button">Добавить</span></a>
            <#if location.customers?has_content>
                <table id="customers_table" class="table">
                    <thead>
                    <tr>
                        <td class="hidden">id</td>
                        <td>Фамилия</td>
                        <td>Имя</td>
                        <td>Отчество</td>
                        <td>Дата рождения</td>
                        <td>Почта</td>
                        <td>Телефон</td>
                        <td>Руководитель</td>
                    </tr>
                    </thead>
                    <tbody>
                        <#list customers as customer>
                        <tr>
                            <td id="id" class="hidden">${customer.id}</td>
                            <td id="lastname">${customer.lastname}</td>
                            <td id="firstname">${customer.firstname}</td>
                            <td id="secondname"><#if customer.secondname??>${customer.secondname}</#if></td>
                            <td id="birth">${customer.birth?string["dd.MM.yyyy"]}</td>
                            <td id="email"><#if customer.email??>${customer.email}</#if></td>
                            <td id="phone">${customer.phone}</td>
                            <td id="ownedDepartment"><#if customer.ownedDepartment??>${customer.ownedDepartment.name}</#if></td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            <#else>
                <div class="col-md-12"><p>Сотрудники отсутствуют</p></div>
            </#if>
        <#else>
           <div class="col-md-12"><p>Выберите отдел</p></div>
        </#if>
        </div>
    </div>
</body>
</html>