<html>
<head><#include "head.ftl"/>
<body>
<#include "location_tree.ftl"/>
<#include "location_info.ftl"/>
<div class="col-md-8 half-hght">
    <div class="page-header">
        <h3 id="updateCustomerLable">Добавление сотрудника</h3>
        <p>Заполните поля формы</p>
    </div>
    <div class="form-wrapper">
            <form id="updateCustomerInfo" name="updateCustomerInfo" action="/structure/customer/update" method="POST">
                <div class="form-group hidden">
                    <label for="id" class="form-control-label">id:</label>
                    <input type="text" name="id" class="form-control" id="id" value="<#if (customerUpdate.id)?? >${customerUpdate.id}</#if>">
                </div>
                <div class="form-group hidden">
                    <label for="department" class="form-control-label">department:</label>
                    <input type="text" name="department" class="form-control" id="department" value="<#if (customerUpdate.department.id)?? >${customerUpdate.department.id}</#if>">
                </div>
                <div class="form-group">
                <@spring.bind "customerUpdate.lastname"/>
                    <label for="lastname" class="form-control-label">Фамилия:</label>
                <@spring.formInput path="customerUpdate.lastname" attributes="required='required' class='form-control'"/>
                <@spring.showErrors classOrStyle="error" separator="<br>" />
                </div>
                <div class="form-group">
                <@spring.bind "customerUpdate.firstname"/>
                    <label for="firstname" class="form-control-label">Имя:</label>
                <@spring.formInput path="customerUpdate.firstname" attributes="required='required' class='form-control'"/>
                <@spring.showErrors classOrStyle="error" separator="<br>" />
                </div>
                <div class="form-group">
                <@spring.bind "customerUpdate.secondname"/>
                    <label for="secondname" class="form-control-label">Отчество:</label>
                <@spring.formInput path="customerUpdate.secondname" attributes="class='form-control'"/>
                <@spring.showErrors classOrStyle="error" separator="<br>" />
                </div>
                <div class="form-group">
                    <label for="birth" class="form-control-label">Дата рождения:</label>
                    <input type="date" name="birth" class="form-control" id="birth" value="<#if (customerUpdate.birth)?? >${customerUpdate.birth?string["yyyy-MM-dd"]}</#if>">
                </div>
                <div class="form-group">
                <@spring.bind "customerUpdate.email"/>
                    <label for="email" class="form-control-label">E-mail:</label>
                    <input type="email" name="email" class="form-control" id="email" value="<#if (customerUpdate.email)?? >${customerUpdate.email}</#if>">
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                </div>
                <div class="form-group">
                <@spring.bind "customerUpdate.phone"/>
                    <label for="phone" class="form-control-label">Номер телефона:</label>
                <@spring.formInput path="customerUpdate.phone" attributes="class='form-control'"/>
                <@spring.showErrors classOrStyle="error" separator="<br>" />
                </div>
            </form>
            <a class="btn btn-default" href="/structure/office/department?id=${locationInfo.id}">Отмена</a>
            <button type="submit" class="btn btn-primary pull-right" form="updateCustomerInfo" value="Сохранить"><span role="button" title="Сохранить">Сохранить</span></button>
    </div>
</div>
</body>
</html>