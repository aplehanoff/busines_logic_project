<div class="col-md-4 full-hght location-tree">
    <div class="col-md-12">
        <div class="col-md-10 title h2">
            <a href="/structure/">Дерево отделов</a>
        </div>
        <div class="col-md-2">
            <a href="/structure/office/add" title="Добавить офис" type="button" class="add btn btn-default pull-right" ><span role="button">Добавить офис</span></a>
        </div>
    </div>
    <div class="col-md-12">
        <ul class="nav nav-list">
            <li class="divider"></li>
        <#list offices as office>
            <li id="${office.id}">
                <a class="nav-header" href="/structure/office?id=${office.id}">${office.name}</a>
                <ul class="inner nav nav-list tree">
                    <#if office.departments??>
                        <#list office.departments as depart>
                            <#if !depart.department??>
                                <#macro deptree depart>
                                    <li class="" id="${depart.id}" data-source="department">
                                        <a href="/structure/office/department?id=${depart.id}">${depart.name}</a>
                                        <ul class="inner nav nav-list tree">
                                            <#if depart.subDepartments??>
                                                <#list depart.subDepartments as sub>
                                                <@deptree sub />
                                            </#list>
                                            </#if>
                                        </ul>
                                    </li>
                                </#macro>
                                <@deptree depart/>
                            </#if>
                        </#list>
                    </#if>
                </ul>
            </li>
            <li class="divider"></li>
        </#list>
        </ul>
    </div>
</div>