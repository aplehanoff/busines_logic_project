<html>
<head><#include "head.ftl"/>
<body>
<#include "location_tree.ftl"/>
    <div class="col-md-8 half-hght">
        <div class="page-header">
            <h3 id="updateCustomerLable">Добавление офиса</h3>
            <p>Заполните поля формы</p>
        </div>
        <div class="form-wrapper">
            <div id="addOffice" aria-labelledby="addOfficeLable" role="dialog" tabindex="-1">
                <form id="customer" name="office" action="/structure/office/add" data-toggle="validator" method="POST">
                    <div class="form-group">
                    <@spring.bind "addOffice.name"/>
                        <label for="name" class="form-control-label">Наименование:</label>
                    <@spring.formInput path="addOffice.name" attributes="class='form-control' required='required'"/>
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                    </div>
                    <div class="form-group">
                    <@spring.bind "addOffice.address"/>
                        <label for="address" class="form-control-label">Адрес:</label>
                    <@spring.formTextarea path="addOffice.address" attributes="class='form-control' required='required'"/>
                    </div>
                </form>
                <a class="btn btn-default" href="/structure/">Отмена</a>
                <button type="submit" class="btn btn-primary" form="customer" value="Создать">Создать</button>
            </div>
        </div>
    </div>
</body>
</html>