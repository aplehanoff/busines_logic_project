<html>
<head><#include "head.ftl"/>
<body>
    <#include "location_tree.ftl"/>
    <#include "location_info.ftl"/>
    <div class="col-md-8 half-hght">
        <div class="page-header">
            <h3 id="updateCustomerLable">Добавление сотрудника</h3>
            <p>Заполните поля формы</p>
        </div>
        <div class="form-wrapper">
            <div id="addCustomer" aria-labelledby="addCustomerLable" role="dialog" tabindex="-1">
                <form id="customerAdd" name="customerAdd" action="/structure/customer/add" method="POST">
                    <div class="form-group">
                        <div class="<#if !locationInfo.owner??>col-md-8</#if>">
                            <label for="departmentName" class="form-control-label">Отдел:</label>
                            <input type="text" class="hidden" name="departmentId" readonly="readonly" id="departmentId" value="${locationInfo.id}">
                            <input type="text" class="form-control" readonly="readonly" id="departmentName" value="${locationInfo.name}">
                        </div>
                        <#if !locationInfo.owner??>
                            <div class="col-md-4">
                                <label for="owner" class="form-control-label">Руководитель:</label>
                                <input type="checkbox" name="owner" class="form-control" id="owner" >
                            </div>
                        </#if>
                    </div>
                    <div class="form-group">
                    <@spring.bind "addCustomer.lastname"/>
                        <label for="lastname" class="form-control-label">Фамилия:</label>
                        <@spring.formInput path="addCustomer.lastname" attributes="required='required' class='form-control'"/>
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                    </div>
                    <div class="form-group">
                    <@spring.bind "addCustomer.firstname"/>
                        <label for="firstname" class="form-control-label">Имя:</label>
                        <@spring.formInput path="addCustomer.firstname" attributes="required='required' class='form-control'"/>
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                    </div>
                    <div class="form-group">
                    <@spring.bind "addCustomer.secondname"/>
                        <label for="secondname" class="form-control-label">Отчество:</label>
                        <@spring.formInput path="addCustomer.secondname" attributes="class='form-control'"/>
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                    </div>
                    <div class="form-group">
                    <@spring.bind "addCustomer.birth"/>
                        <label for="birth" class="form-control-label">Дата рождения:</label>
                        <input type="date" required="required" name="birth" class="form-control" id="birth">
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                    </div>
                    <div class="form-group">
                    <@spring.bind "addCustomer.email"/>
                        <label for="email" class="form-control-label">E-mail:</label>
                        <input type="email" name="email" class="form-control" id="email">
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                    </div>
                    <div class="form-group">
                    <@spring.bind "addCustomer.phone"/>
                        <label for="phone" class="form-control-label">Номер телефона:</label>
                        <@spring.formInput path="addCustomer.phone" attributes="class='form-control'"/>
                    <@spring.showErrors classOrStyle="error" separator="<br>" />
                    </div>
                </form>
                <a class="btn btn-default" href="/structure/office/department?id=${locationInfo.id}">Отмена</a>
                <button type="submit" class="btn btn-primary" form="customerAdd" value="Создать">Создать</button>
            </div>
        </div>
    </div>
</body>
</html>