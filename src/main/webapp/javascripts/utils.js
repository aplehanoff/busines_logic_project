$(document).ready(function () {
    $("#customers_table tr").click(function () {
        if($(this).hasClass("active"))
            $(this).removeClass("active");
        else {
            $("#customers_table tr").each(function (index, element) {
                $(element).removeClass("active");
            });
            $(this).addClass("active");
        }
    });

   $("span.error").prev(".form-control").addClass("error");
   $(".form-control").change(function () {
       $(this).next("span.error").remove();
       $(this).removeClass("error");
   })
});