/**
 * Create/Update requests
 */

this.updateCustomerGet = function() {
    var id_selected = $('#customers_table .active #id').text();
    if(id_selected != "") {
        location.replace("/structure/customer/update?id=" + id_selected);
    }
};

this.removeCustomer = function () {
    var id_deleted = $('#customers_table .active #id').text();
    if(id_deleted != ""){
        $.post("/structure/customer/delete?id="+id_deleted, function () {
            location.reload();
        })
    }
};